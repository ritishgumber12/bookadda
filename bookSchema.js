var mongoose=require("mongoose");
var bookSchema={
name:{required:true,
type:String
},
price:{
type:Number,
required:true,default:0},
soldBy:{
type:String,
required:true
},
postedOn:{
type:String,
required:true
},
status:{
type:Boolean,
required:true,
default:false}
};
var schema=new mongoose.Schema(bookSchema);
module.exports=schema;
module.exports.bookSchema=schema;