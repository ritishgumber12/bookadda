var mongoose = require("mongoose");
var userSchema = {
	name : {
		required : true,
		type : String
	},
	email : {
		type : String,
		required : true,
		unique : true,
		sparse : true
	},
	cno : {
		required : true,
		type : Number
	},
	password : {
		required : true,
		type : String
	},
	img : {
		data : Buffer,
		contentType : String
	}
};
var schema = new mongoose.Schema(userSchema);
module.exports = schema;
module.exports.userSchema = schema;