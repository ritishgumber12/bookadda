var express = require("express");
var mongoose = require("mongoose");
console.log("b4 uri");
mongoose.connect("mongodb://localhost:27017/SaveBook");
console.log("a4 uri");
var Book = mongoose.model("Book", require("./bookSchema"), "bookss");
var User = mongoose.model("User", require("./userSchema"), "users");
var fs=require("fs");

console.log("a4 model");
module.exports = function() {
	var app = express();
	app.use(express.static('public'));

	app.engine('html', require('ejs').renderFile);

	app.get('/', function(req, res) {
		res.render("./login.html");
	});
	app.get('/enquiry', function(req, res) {
		res.render("./enquiry.html");
	});
	app.get('/addBook', function(req, res) {
		res.render("./addBook.html");
	});
	app.get('/bookEnquiry', function(req, res) {
res.send('');	});
	

	app.get('/test', function(req, res) {
		res.render("./test.html");
	});
	app.get('/login', function(req, res) {
		res.render("./login.html");
	});
	app.get('/register', function(req, res) {
		res.render("./register.html");
	});
	app.post('/addUser', function(req, res) {
		User.count({
			email : (req.query.email).toLowerCase()
		}, function(err, c) {
			if (c == 0) {
			
				var imgPath=req.query.img;
				
				var user = new User({
					name : req.query.name,
					email : (req.query.email).toLowerCase(),
					cno : req.query.cno,
					password : req.query.pass1
				});
				user.img.data=fs.readFileSync(imgPath);
				user.img.contentType='jpg';
				user.save(function(error) {
					if (error) {
						console.log(error);
						process.exit(1);
					}
				});
				res.send("successfully Added <br>Name:" + req.query.name);
			} else {
				res.send("User already Exists");
			}
		});
	});

	app.get("/searchBook", function(req, res) {

		res.render("./search.html");
	});
	
	app.get("/profile", function(req, res) {

		res.render("./userProfile.html");
	});
	
	app.get("/allRecords", function(req, res) {

		res.render("./all.html");
	});
	app.get("/save", function(req, res) {
var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var todayy = dd+'/'+mm+'/'+yyyy;
		var book = new Book({
			name : req.query.name,
			price : req.query.price,
			soldBy  : req.query.email,
			postedOn : todayy
			

		});
		book.save(function(error) {
			if (error) {
				console.log(error);
				process.exit(1);
			}
					res.send("successfully Added");

		});

	});

	app.get("/all", function(req, res) {
		var ski = req.query.skip;
		var pageSi = req.query.pageSize;
		var x = (req.query.searchText).split(" ");
		console.log(x);
		var patt = "(.*)";
		for (var i = 0; i < x.length; i++)
			patt += x[i] + "(.*)";
		var countt;
		console.log(patt);

		Book.find({
			name : {
				$regex : patt,
				$options : "$i"
			}
		}, function(error, docs) {
			res.header("Access-Control-Allow-Origin", "*");
			console.log(docs);
			res.send(docs);
		}).skip(parseInt(ski)).limit(parseInt(pageSi));
	});

	app.get('/checkUser', function(req, res) {

		User.findOne({
			email : (req.query.email).toLowerCase()
		}, function(err, docs) {
			res.header("Access-Control-Allow-Origin", "*");
			if (docs != null){
				if(docs.password==req.query.password){
					res.send({valid:true});
					}
				else{res.send({valid:false});}					
				}
			else {res.send({valid:false});}
		});

	});
	
	
	
	app.get("/getUser",function(req,res){
User.findOne({
			email : (req.query.email).toLowerCase()
		}, function(err, docs) {
			res.header("Access-Control-Allow-Origin", "*");
			res.send(docs);
		});	
	});

	app.get("/count", function(req, res) {
		var x = (req.query.searchText).split(" ");
		var patt = "(.*)";
		for (var i = 0; i < x.length; i++)
			patt += x[i] + "(.*)";
		Book.count({
			name : {
				$regex : patt,
				$options : "$i"
			}
		}, function(err, c) {
			res.send(c + " ");
			console.log(c);

		});

	});

	return app;
};